import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseLoginBMI'), [('userid') : GlobalVariable.UserId, ('password') : GlobalVariable.Password], 
    FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/1MenuMore2-HomePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/1MenuMore2-HomePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/1MenuMore2-HomePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.swipe(553, 1508, 585, 910, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/2MenuTelcoPrepaid-MorePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/3EditTextPhoneNumber-TelcoPrepaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/3EditTextPhoneNumber-TelcoPrepaidPage'), GlobalVariable.Xl, 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/4ButtonNext-TelcoPrepaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Xl/25000-NominalPulsa'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/7ButtonPayNow-PaymentConfPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn1-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn4-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn1-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn4-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn1-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/8Btn4-MobilePinPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/BtnDone-EslipDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/BtnDone-EslipDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/14BtnDone-ESlipDetailPage'), 0)
Mobile.pressBack(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/LoginPage/PopUpLogoutVerify'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/LoginPage/YesLogoutBtn-LogoutPopUp'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Activation/20-LoginPageVerify'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.pressBack(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.pressBack(FailureHandling.CONTINUE_ON_FAILURE)

