import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseLoginBMI'), [('userid') : GlobalVariable.UserId, ('password') : GlobalVariable.Password], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CIndosat'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CSmartfren'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CMentari'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CThree'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CXl'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/MenujuIsiNomor'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Purchase/TelcoPrepaid/Case/CTelkomsel'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseLogoutBMI'), [:], FailureHandling.STOP_ON_FAILURE)

