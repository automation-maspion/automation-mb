import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.comment('==========Indosat==========')

Mobile.setText(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/3EditTextPhoneNumber-TelcoPrepaidPage'), GlobalVariable.Indosat, 
    0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/1.png')

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/4ButtonNext-TelcoPrepaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/2.png')

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Indosat/30000-NominalPulsa'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/3.png')

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/7ButtonPayNow-PaymentConfPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/4.png')

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseMPinBMI'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/5.png')

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/BtnDone-EslipDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/BeliPulsa/Indosat/6.png')

Mobile.verifyElementExist(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/BtnDone-EslipDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BeliPulsa/Smartfren/BtnDone-EslipDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

