import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Payment/1MenujuPayment'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/0-BtnTelcoPostpaid-PaymentPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TelcoPostPaid/1-VerifyTelcoPospaidPage-0'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/1-BtnPilihProvider-TelcoPospaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/2-BtnKartuHalo-TelcoPosPaidPage'), 0)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/3-EditTextPhoneNumber-TelcoPosPaidPage'), 0)

Mobile.setText(findTestObject('BMI/Transaksi/TelcoPostPaid/3-EditTextPhoneNumber-TelcoPosPaidPage'), GlobalVariable.HaloTsel, 
    0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/4-BtnNext-TelcoPosPaidPage'), 0)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TelcoPostPaid/5-VerifyTelcoPosPaidPage-1'), 0)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/5-BtnPayNow-TelcoPosPaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseMPinBMI'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TelcoPostPaid/6-VerifyTelcoPosPaidPage-2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//String saya = Mobile.getAttribute(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)
Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/TelcoPostPaid/6-VerifyTelcoPosPaidPage-2'), 'contentDescription', 
    'Success', 0, FailureHandling.CONTINUE_ON_FAILURE)

//String saya2 = Mobile.getAttribute(findTestObject('BMI/Transaksi/BPJSKesehatan/6-VerifyBPJSKesPage-3'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)
Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/TelcoPostPaid/6-VerifyTelcoPosPaidPage-2-1'), 'contentDescription', 
    'SUCCESS', 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TelcoPostPaid/6-BtnDone-TelcoPosPaidPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

