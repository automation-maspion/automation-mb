import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.MobileElement as MobileElement
import io.appium.java_client.android.AndroidDriver as AndroidDriver

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Payment/1MenujuPayment'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/1-BtnPDAM-MorePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/PDAM/AETRA/2-VerifyPDAM-PDAMPage-1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/BtnInstitution'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/BtnAETRA'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/4-EditTextCustId-PDAMPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('BMI/Transaksi/PDAM/AETRA/4-EditTextCustId-PDAMPage'), GlobalVariable.AETRA, 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/5-BtnNext-PDAMPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/PDAM/AETRA/6-VerifyPDAM-2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/6-BtnPayNow-PDAMPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseMPinBMI'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//String saya = Mobile.getAttribute(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3'), 'contentDescription', 'Success', 0, FailureHandling.CONTINUE_ON_FAILURE)

String saya2 = Mobile.getAttribute(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3-1'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3-1'), 'contentDescription', saya2, 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/PDAM/AETRA/7-BtnDone-PDAMPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

