import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/Payment/1MenujuPayment'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/0-BtnBPJSKesehatan-PaymentPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BPJSKesehatan/1-VerifyBPJSKesehatanPage-0'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/1-EditTextVirtualAccNum-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('BMI/Transaksi/BPJSKesehatan/1-EditTextVirtualAccNum-BPJSKesPage'), GlobalVariable.BPJSKesehatan, 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/2-BtnJumlahBulan-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/3-Btn1Bulan-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/4-BtnNext-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BPJSKesehatan/5-VerifyBPJSKesehatanPage-1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/5-BtnPayNow-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseMPinBMI'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/BPJSKesehatan/6-VerifyBPJSKesPage-2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//String saya = Mobile.getAttribute(findTestObject('BMI/Transaksi/PDAM/AETRA/7-VerifyPDAM-3'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)
Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/BPJSKesehatan/6-VerifyBPJSKesPage-2'), 'contentDescription', 'Success', 
    0, FailureHandling.CONTINUE_ON_FAILURE)

//String saya2 = Mobile.getAttribute(findTestObject('BMI/Transaksi/BPJSKesehatan/6-VerifyBPJSKesPage-3'), 'contentDescription', 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementAttributeValue(findTestObject('BMI/Transaksi/BPJSKesehatan/6-VerifyBPJSKesPage-3'), 'contentDescription', 'SUCCESS', 
    0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/BPJSKesehatan/7-BtnDone-BPJSKesPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

