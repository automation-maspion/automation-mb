import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('BMI/Positif/Transaksi/TopUp/MenujuTopUp'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/1-BtnTopUpShopeePay-MorePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/1.png')

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/1-BtnTopUpShopeePay-MorePage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/2-VerifyTopUpShopeePayPage-1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/2-EditText-TopUpShopeePayPage'), 0)

Mobile.sendKeys(findTestObject('BMI/Transaksi/TopUp/ShopeePay/2-EditText-TopUpShopeePayPage'), GlobalVariable.ShopeePay)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/2.png')

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/3-BtnNext-TopUpShopeePayPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/4-VerifyTopUpShopeePayPage-2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/3.png')

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/4-20000Nominal-TopUpShopeePayPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/5-VerifyTopUpShopeePayPage-3'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/4.png')

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/5-BtnTopUp-TopUpShopeePayPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/5.png')

WebUI.callTestCase(findTestCase('BMI/BaseTestCases/BaseMPinBMI'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/6-VerifyTopUpShopeePayPage-4'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/Transaksi/TopUp/ShopeePay/6-VerifyTopUpShopeePayPage-4-1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.takeScreenshot('Picture/Positif/TopUp/ShopeePay/6.png')

Mobile.tap(findTestObject('BMI/Transaksi/TopUp/ShopeePay/6-BtnDone-TopUpShopeePayPage'), 0, FailureHandling.CONTINUE_ON_FAILURE)

