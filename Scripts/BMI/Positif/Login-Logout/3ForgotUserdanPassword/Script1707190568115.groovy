import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.pactindo.revamp.bmi', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('BMI/LoginPage/TransferFeature'), 6)

Mobile.tap(findTestObject('BMI/LoginPage/TransferFeature'), 0)

Mobile.verifyElementVisible(findTestObject('BMI/LoginPage/UserIDField'), 0)

Mobile.tap(findTestObject('BMI/LoginPage/ForgotUserIdorPasswordBtn-LoginPage'), 0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/ForgotPasswordBtn-AccountRecovPage'), 0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/AccountNumberEditText-ForgotPasswordPage'), 0)

Mobile.setText(findTestObject('BMI/LoginPage/0ForgotPassword/AccountNumberEditText-ForgotPasswordPage'), GlobalVariable.AccountNumber, 
    0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/PhoneNumberEditText-ForgotPasswordPage'), 0)

Mobile.setText(findTestObject('BMI/LoginPage/0ForgotPassword/PhoneNumberEditText-ForgotPasswordPage'), GlobalVariable.PhoneNumber, 
    0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/NIKEditText-ForgotPasswordPage'), 0)

Mobile.setText(findTestObject('BMI/LoginPage/0ForgotPassword/NIKEditText-ForgotPasswordPage'), GlobalVariable.NIK, 0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/verifyForgotPasswordPage'), 0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/VerifyBtn-ForgotPasswordPage'), 0)

Mobile.verifyElementVisible(findTestObject('BMI/LoginPage/0ForgotPassword/VerifyOTPPage'), 0)

Mobile.comment('====================================================================')

while (!Mobile.verifyElementVisible(findTestObject('BMI/LoginPage/0ForgotPassword/IzinkanBtn-OTPotomatis'), 0)) {
    Mobile.delay(63, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/ResendOTP-OTPPage'), 0)
}

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/IzinkanBtn-OTPotomatis'), 0)

Mobile.tap(findTestObject('BMI/LoginPage/0ForgotPassword/NextBtn-OTPPage'), 0)

Mobile.sendKeys(findTestObject('BMI/LoginPage/0ForgotPassword/PasswordEditText-CreateNewPassPage'), 'Syahrir125690#')

Mobile.sendKeys(findTestObject('BMI/LoginPage/0ForgotPassword/ConfirmPassword-CreateNewPassPage'), 'Syahrir125690#')

